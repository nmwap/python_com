# python .com爬虫

#### 项目介绍
Python编写的爬虫，爬取.com域名注册信息情况


#### 参数说明

``` bash
	安装库
	itertools
	运行
	python myThread.py
    
```

#### 说明

>  数据来源www.aliyun.com

>  如果对您有帮助，您可以点右上角 "Star" 支持一下 谢谢！ ^_^

>  或者您可以 "follow" 一下，我会不断开源更多的有趣的项目

>  开发环境 w7  Chrome 61 vscode

>  如有问题请直接在 Issues 中提，或者您发现问题并有非常好的解决方案，欢迎 PR 👍